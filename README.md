# I. Setup sublime/mm project with git: #
	1. Create a mavensmate project with your sandbox
	2. Move the mm project folder to desktop(any other location other than the current folder)
	3. install SourceTree > "Clone/New":
	   URL: https://bitbucket.org/besharps/onboarding.git
	   Use your mm project folder as the destination path
	4. Copy back the following files from your mm project folder previously moved
	   config/*
	   [mm Project Name].sublime-project

# II. Commit and pull request #
	1. The current branch is master
	2. All commits to this branch have to go through pull requests
	3. Check in to master branch and pull to make sure it is up to date begin each new development
	4. Create your own branch for User Story:
	5. Once User Story is done, make pull request to merge into master
	   (make sure to select “Create Pull Request” option from “Commit options” dropdown when you do the first commit, this will redirect you to bitbucket and finish the pull request set up )
	6. Once it's approved and merged, your branches are removed from cloud repo, but you still have it locally
	7. when to push - milestone, end of development
	8. what to push - all metadata related to the current development, no more no less. dependencies.
	9. how to verify - deployCheck

# III. Metadata creating standard #
	1. Configuration components must have description field filled with meanful info, no exception
		a. Custom objects -- include Custom Setting and Custom metadata type
		b. Custom fields -- help text should be provided especially on Custom setting and Custom metadata type
		c. Workflow rules, Email alert, Field update, process builder
		d. Validation rules, Duplication rules, Matching rules
		...
	2. Customized components must follow naming convention
		a. Apex, VF, component, Lightning component, staticresource need to have the project prefix+'_'. ex, OB_testingCltr.cls
		b. Custom Setting and Custom metadata type need to have the project prefix+'_'. ex, OB_testing_setting
		c. Custom app, queue, role need to have the project prefix+'_'. ex, OB_testing.app
		d. Email template, letterhead need to have the project prefix+'_'. ex, OB_testing.app
		e. Document, report, dashboard, email's folder name need to use the project prefix. ex, OB folder for email.
		...
# IV. headers -- Test class only need class header. Use a semantic method name for self-explain. #
	1. Apex Class Header:
	/**
	* @File Name    :   OB_EventsCtlr
	* @Description  :   server side event controller
	* @Date Created :   10/14/2017
	* @Author       :   XXX
	* @group        :   controller
	* @Modification Log:
	*******************************************************************************
	* Ver       Date        Author                  Modification
	* 1.0       10/14/2017  XXX          Created the file/class
	* 1.1       11/09/2017  XXX     CPP-293 Add/Remove Events to Associated Trip
	**/
	
	2. Apex Method Header:
		/**
	   * @Name          updateCounsellorInfo
	   * @Description   Update current login Counsellor Informations
	   * @Author        XXX
	   * @CreatedDate   2017-09-29
	   * [@Parameter]     JSON OB_CounsellorWrapper
	   * [@Return]       String
	   */
	   
	3. Lightning Cmp:
	<!--
	* @File Name    :   OB_EventsCtlr
	* @Description  :   cmp
	* @Date Created :   10/14/2017
	* @Author       :   XXX
	* @group        :   cmp
	* @Modification Log:
	*******************************************************************************
	* Ver       Date        Author                  Modification
	* 1.0       10/14/2017  XXX          Created the file/class                     
	-->
	
	4. Lightning Cmp Controller/helper:
	/**
	* @File Name:   OB_AddEventsToTripController.js
	* @Description: Javascript controller used to calls methods in OB_AddEventsToTripHelper.
	* @Author:      XXX
	* @CreatedDate   : 2017-11-09 
	*/

# v. Delete metadata cautions -- add the metadata tags in the src/destructiveChanges.xml #
	if you need to delete the following metadata types:
	1. object or field
	2. Tab
	3. App
	4. Apex class
	5. Visualforce page
	6. Page layouts
	7. Record types
	you need to make sure you remove those tags from all profiles and permission sets.
	please reach out to your release manager if you have any question.