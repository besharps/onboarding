/**
* @File Name:   DD_TriggerConfig.cls
* @Description:   
* @Author:      Fan Yang, fanxyang@deloitte.ca
* @Group:       Apex
* @Last Modified by:   Fan Yang
* @Last Modified time: 2017-05-09 16:42:30
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver       Date        Author      Modification
* 1.0       2017-05-09  Fan Yang    Created the file/class
*/
public with sharing class DD_TriggerConfig {
    public SObjectType SObjectType{get; private set;}
    public Boolean IsActive{get; private set;}
    public String Handler{get;private set;}
    public String SObjectName{get;private set;}
    public static Map<String, DD_TriggerConfig> Configurations{//All Trigger configurations
        get{
            if(Configurations == null){
                Configurations = new Map<String, DD_TriggerConfig>();
                for(Trigger_Config__mdt cfg : [
                    SELECT Active__c,Handler_Name__c,
                    SObject__c,DeveloperName,Id,MasterLabel 
                    FROM Trigger_Config__mdt]
                ){
                    DD_TriggerConfig tc = new DD_TriggerConfig();
                    tc.IsActive = cfg.Active__c;
                    tc.SObjectName = cfg.SObject__c;
                    tc.SObjectType = Schema.getGlobalDescribe().get(tc.SObjectName);
                    tc.Handler = cfg.Handler_Name__c;
                    Configurations.put(tc.SObjectName, tc);
                }
                
            }
            return Configurations;
        }
        private set;
    }

    /**
    * @Name          DD_TriggerConfig
    * @Description   Private Constructor of class
    * @Author        Fan Yang
    * @CreatedDate   2017-05-09
    * @Param         String devName
    * @Return        N/A
    */
    private DD_TriggerConfig() {}

    
    /**
    * @Name          getBySObjectName
    * @Description   Get Trigger Configuration By Name
    * @Author        Fan Yang
    * @CreatedDate   2017-05-09
    * @Param         String Name
    * @Return        DD_TriggerConfig
    */
    public static DD_TriggerConfig getBySObjectName(String Name){
        return Configurations.get(Name);
    }

    /**
    * @Name          getBySObjectType
    * @Description   Get Trigger Configuration By SObject Type
    * @Author        Fan Yang
    * @CreatedDate   2017-05-09
    * @Param         SObjectType Type
    * @Return        DD_TriggerConfig
    */
    public static DD_TriggerConfig getBySObjectType(SObjectType Type){
        return Configurations.get(String.valueOf(Type));
    }
}