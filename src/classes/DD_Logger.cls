/**
* @File Name:   DD_Logger.cls
* @Description: Logger class to create a record in Custom Object to track down errors and other informations
*               Example A: 
*               new DD_Logger().log('Trace Information').push();
*
*               Example B: 
*               new DD_Logger().log(ex).pushAsync();//push in future
*
* @Author:      Fan Yang, fanxyang@deloitte.ca
* @Group:       Apex
* @Last Modified by:   Fan Yang
* @Last Modified time: 2018-02-03 10:29:46
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver       Date        Author      Modification
* 1.0       2017-05-21  Fan Yang    Created the file/class
*/
public with sharing class DD_Logger {
    private final static integer MAX_LENGTH_DESCRIPTION = 32768;//max length of description
    private final static integer MAX_LENGTH_STACK_TRACE = 32768;//max length of stack trace
    private final static integer MAX_LENGTH_COMMENT = 1000;//max length of comment
    public enum SEVERITY_TYPES {ERROR, CONFIRM, FATAL, INFO, WARNING}//types of log
    public static SEVERITY_TYPES DEFAULT_LOG_TYPE = SEVERITY_TYPES.ERROR;//default type of log
    private static final Boolean ENABLE_LOGS = true;//switch for all logs, can be associate with a custom setting
    private List<sObject> lsLogs = new List<sObject>();//caching for DML
    
    public DD_Logger(){}

    /**
    * @Name          add
    * @Description   Add new log
    * @Author        Fan Yang
    * @CreatedDate   2018-02-01
    * @Param         Exception e
    * @Return        DD_Logger
    */
    public DD_Logger log(Exception e){
        Log l = new Log();
        l.ExceptionObj = e;
        l.Severity = DEFAULT_LOG_TYPE;
        lsLogs.add(l.createSystemLog());
        return this;
    }

    /**
    * @Name          add
    * @Description   Add new log
    * @Author        Fan Yang
    * @CreatedDate   2018-02-01
    * @Param         String comments
    * @Return        DD_Logger
    */
    public DD_Logger log(String comments){
        Log l = new Log();
        l.Comments = comments;
        l.Severity = SEVERITY_TYPES.INFO;
        lsLogs.add(l.createSystemLog());
        return this;
    }

    /**
    * @Name          add
    * @Description   Add new log
    * @Author        Fan Yang
    * @CreatedDate   2018-02-01
    * @Param         Exception e
    * @Param         Type t   Apex Class
    * @Return        DD_Logger
    */
    public DD_Logger log(Exception e, Type t){
        Log l = new Log();
        l.ClassObject = t;   
        l.ExceptionObj = e;
        l.Severity = DEFAULT_LOG_TYPE;
        lsLogs.add(l.createSystemLog());
        return this;
    }

    /**
    * @Name          log
    * @Description   Add a new log
    * @Author        Fan Yang
    * @CreatedDate   2018-02-02
    * @Param         Exception e
    * @Param         Type t
    * @Param         String sourceId
    * @Return        DD_Logger
    */
    public DD_Logger log(Exception e, String sourceId, Type t){
        Log l = new Log();
        l.ClassObject = t;   
        l.ExceptionObj = e;
        l.Severity = DEFAULT_LOG_TYPE;
        l.SourceId = sourceId;
        lsLogs.add(l.createSystemLog());
        return this;
    }

    /**
    * @Name          add
    * @Description   Add new log
    * @Author        Fan Yang
    * @CreatedDate   2018-02-01
    * @Param         Exception e
    * @Param         Type t   Apex Class
    * @Param         String category
    * @Return        DD_Logger
    */
    public DD_Logger log(Exception e, Type t, String category){
        Log l = new Log();
        l.ClassObject = t;   
        l.Severity = DEFAULT_LOG_TYPE;
        l.ExceptionObj = e;
        l.Category = category;
        lsLogs.add(l.createSystemLog());
        return this;
    }

    /**
    * @Name          add
    * @Description   Add new log
    * @Author        Fan Yang
    * @CreatedDate   2018-02-01
    * @Param         Exception e
    * @Param         Type t   Apex Class
    * @Param         String category
    * @Param         SEVERITY_TYPES type
    * @Param         String sourceId
    * @Return        DD_Logger
    */
    public DD_Logger log(Exception e, Type t, String category, SEVERITY_TYPES type, String sourceId){
        Log l = new Log();
        l.SourceId = sourceId;   
        l.ClassObject = t;   
        l.Severity = type;   
        l.ExceptionObj = e;
        l.Category = category;
        lsLogs.add(l.createSystemLog());
        return this;
    }

    /**
    * @Name          save
    * @Description   Commit the logs into database
    * @Author        Fan Yang
    * @CreatedDate   2018-02-01
    * @Return        void
    */
    public void push(){
        if(ENABLE_LOGS == false)
            return;

        if(lsLogs.size() > 0){
            insert lsLogs;
        }

        this.flush();
    }

    /**
    * @Name          pushAsync
    * @Description   Save log to Salesforce asynchronously
    * @Author        Fan Yang
    * @CreatedDate   2018-02-01
    * @Return        void
    */
    public void pushAsync(){
        if(ENABLE_LOGS == false)
            return;


        if(lsLogs.size() > 0){
            List<String> jsonStrs = new List<String>();
            for(sObject obj : lsLogs){
                DD_Error_Log__c sLog = (DD_Error_Log__c)obj;
                jsonStrs.add(JSON.serialize(sLog));
            }
            insertFuture(jsonStrs);
        }

        this.flush();
    }

    /**
    * @Name          flush
    * @Description   flush the cache
    * @Author        Fan Yang
    * @CreatedDate   2018-02-01
    * @Return        void
    */
    private void flush(){
        lsLogs.clear();
    }

    /**
    * @Name          insertFuture
    * @Description   Insert sobject in asynchronous mode
    * @Author        Fan Yang
    * @CreatedDate   2017-05-21
    * @Param         string jsonStr
    * @Return        void
    */
    @future
    private static void insertFuture(String jsonStr)
    {
        insertFuture(new List<String>{jsonStr});
    }

    /**
    * @Name          insertFuture
    * @Description   Insert list of sobject in asynchronous mode
    * @Author        Fan Yang
    * @CreatedDate   2018-02-01
    * @Param         List<String> jsonStr
    * @Return        void
    */
    @future
    private static void insertFuture(List<String> jsonStrs)
    {
        List<SObject> toInsert = new List<SObject>();
        for(String j : jsonStrs){
            toInsert.add((SObject)JSON.deserialize(j, sObject.class));
        }
        insert toInsert;
    }

    /**
    * @Name          Log Object
    * @Description   Warpper class to contain log information
    * @Author        Fan Yang
    * @CreatedDate   2018-02-01
    */
    private class Log{
        public Exception ExceptionObj {get;set;}//exception instance
        public String SourceId{get;set;}//source record id
        public Type ClassObject{get;set;}//class type
        public SEVERITY_TYPES Severity{get;set;}//severity type
        public String Category{get;set;}//category of system log
        public String Comments{get;set;}//comment of the log
        /**
        * @Name          createSystemLog
        * @Description   Create and insert system log
        * @Author        Fan Yang
        * @CreatedDate   2017-05-21
        * @Param         DD_Logger sysLog
        * @Return        void
        */
        public DD_Error_Log__c createSystemLog()
        {
            return initializeSystemLog();
        }

        /**
        * @Name          initializeSystemLog
        * @Description   Extract and generate sobject
        * @Author        Fan Yang
        * @CreatedDate   2017-05-21
        * @Param         DD_Logger log
        * @Return        DD_Error_Log__c
        */
        private DD_Error_Log__c initializeSystemLog()
        {
            DD_Error_Log__c oSL = new DD_Error_Log__c();
            
            oSL.Severity__c =  String.valueOf(this.Severity);
            oSL.Source_Id__c = String.isBlank(this.SourceId) ? '' : this.SourceId;
            oSL.Comment__c = String.isBlank(this.Comments) ? '' : truncate(this.Comments, MAX_LENGTH_COMMENT);
            oSL.Category__c = String.isBlank(this.Category) ? 'Default' : this.Category;

            oSL.Apex_Class__c = this.ClassObject == null ? '' :
                String.valueOf(this.ClassObject).split(':')[0];
            
            if (this.exceptionObj != null){
                oSL.Description__c = truncate(this.exceptionObj.getMessage(), MAX_LENGTH_DESCRIPTION);
                oSL.Exception_Type__c = this.exceptionObj.getTypeName();
                oSL.Exception_Stack_Trace__c = truncate(this.exceptionObj.getStackTraceString(), MAX_LENGTH_STACK_TRACE);
            }
            
            return oSL;   
        }

        /**
        * @Name          truncate
        * @Description   truncate log if oversized
        * @Author        Fan Yang
        * @CreatedDate   2017-05-21
        * @Param         string log
        * @Param         integer length
        * @Return        string
        */
        private string truncate(string field, integer length)
        {
            string retVal = field;
            integer maxSize = length;
            
            if (retVal != null)
            {
                if (retVal.length()  >  length)
                    maxSize = length;
                
                retVal = retVal.substring(0, Math.Min(retVal.length(),  maxSize));
            }
            
            return retVal;
            
        }
    }

    /**
    * @Name          LoggerException
    * @Description   Exception Class
    * @Author        Fan Yang
    * @CreatedDate   2018-02-03
    */
    public class LoggerException extends Exception{}

}