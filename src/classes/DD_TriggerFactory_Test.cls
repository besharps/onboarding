/**
* @File Name:   DD_TriggerFactory_Test.cls
* @Description:   
* @Author:      Fan Yang, fanxyang@deloitte.ca
* @Group:       Apex
* @Last Modified by:   Fan Yang
* @Last Modified time: 2017-05-09 17:17:36
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver       Date        Author      Modification
* 1.0       2017-05-09  Fan Yang    Created the file/class
*/
@isTest
private class DD_TriggerFactory_Test
{
    @isTest
    static void itShouldValidateFirstRun()
    {
        Test.startTest();

        DD_ConcreteTriggerHandler handler = new DD_ConcreteTriggerHandler();

        Boolean firstRun = DD_TriggerFactory.ValidateFirstRun(handler);

        System.assert(firstRun);

        Test.stopTest();
    }

    @isTest
    static void itShouldValidateSecondRun()
    {
        Test.startTest();

        DD_ConcreteTriggerHandler handler = new DD_ConcreteTriggerHandler();

        Boolean firstRun = DD_TriggerFactory.ValidateFirstRun(handler);
        Boolean secondRun = DD_TriggerFactory.ValidateFirstRun(handler);

        System.assert(secondRun == false);

        Test.stopTest();
    }

    @isTest
    static void testTriggerImplementation(){
        Test.startTest();
        Contact c = new Contact(LastName = 'Doe');
        Database.insert(c, false);
        c.FirstName = 'Joe';
        Database.update(c, false);
        Database.delete(c, false);
        Test.stopTest();
    }


    private class DD_ConcreteTriggerHandler extends DD_AbstractTriggerHandler{}
}