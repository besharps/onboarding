/**
* @File Name:   DD_Logger_Test.cls
* @Description: Test class for DD_Logger
* @Author:      Fan Yang, fanxyang@deloitte.ca
* @Group:       Apex
* @Last Modified by:   Fan Yang
* @Last Modified time: 2018-02-03 09:29:46
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver       Date        Author      Modification
* 1.0       2017-05-21  Fan Yang    Created the file/class
*/

@isTest
private class DD_Logger_Test
{
    private static final String ACCOUNT_NAME = 'test-acc';
    private static final String SUCCESS = 'success';
    private static final String FAILURE = 'failure';
    
    /**
    * @Name          itShouldCreateSystemLog
    * @Description   verify log creation
    * @Author        Fan Yang
    * @CreatedDate   2018-02-03
    * @Return        void
    */
    @isTest
    static void itShouldCreateSystemLog()
    {
        Test.startTest();
        DD_Logger logger = new DD_Logger();
        logger.log('**************logs**************').push();
        List<DD_Error_Log__c> logs = [SELECT Id FROM DD_Error_Log__c limit 1];
        System.assertEquals(1, logs.size());

        Test.stopTest();
    }

    /**
    * @Name          itShouldCreateSystemLogFuture
    * @Description   Verify create log in future
    * @Author        Fan Yang
    * @CreatedDate   2018-02-03
    * @Return        void
    */
    @isTest
    static void itShouldCreateSystemLogFuture()
    {
        Test.startTest();
        DD_Logger logger = new DD_Logger();
        logger.log('**************logs**************').pushAsync();

        Test.stopTest();

        //inserted in future method, has to assert after Test.stopTest
        List<DD_Error_Log__c> logs = [SELECT Id FROM DD_Error_Log__c limit 1];
        System.assertEquals(1, logs.size());
    }

    /**
    * @Name          itShouldLogError
    * @Description   Test log error
    * @Author        Fan Yang
    * @CreatedDate   2018-02-03
    * @Return        void
    */
    @isTest
    static void itShouldLogError()
    {
        Test.startTest();
        try{
            insert new User(FirstName = 'abc');
        }catch(Exception e){
            DD_Logger logger = new DD_Logger();
            logger.log(e);
            logger.log(e, DD_Logger.class);
            logger.log(e, DD_Logger.class, 'myCat');
            logger.log(e, '500123', DD_Logger.class);
            logger.log(e, DD_Logger.class, 'myCat', DD_Logger.SEVERITY_TYPES.ERROR, null);
            logger.pushAsync();
        }

        Test.stopTest();

        //inserted in future method, has to assert after Test.stopTest
        List<DD_Error_Log__c> logs = [SELECT Id FROM DD_Error_Log__c limit 1000];
        System.assertEquals(5, logs.size());
    }

    /**
    * @Name          testBulkifiedInsert
    * @Description   Test bulkfied log insertion
    * @Author        Fan Yang
    * @CreatedDate   2018-02-03
    * @Return        void
    */
    @isTest
    static void testBulkifiedInsert()
    {
        Test.startTest();
        DD_Logger logger = new DD_Logger();
        logger.log('**************logs 1**************').pushAsync();
        logger.log('**************logs 2**************').pushAsync();
        logger.log('**************logs 3**************').pushAsync();

        Test.stopTest();

        //inserted in future method, has to assert after Test.stopTest
        List<DD_Error_Log__c> logs = [SELECT Id FROM DD_Error_Log__c limit 1000];
        System.assertEquals(3, logs.size());
    }

    @isTest
    static void verifyLogPopulation()
    {
        Test.startTest();
        try{
            insert new User(FirstName = 'abc');//will raise error due to system validation rule
        }catch(Exception e){
            DD_Logger logger = new DD_Logger();
            logger.log(e, DD_Logger.class, 'myCat', DD_Logger.SEVERITY_TYPES.ERROR, '001');
            logger.pushAsync();
        }

        Test.stopTest();//wait future job to finish

        //inserted in future method, has to assert after Test.stopTest
        DD_Error_Log__c log = [
            SELECT 
            Id,Apex_Class__c,Category__c,Comment__c,
            Description__c,Exception_Stack_Trace__c,
            Exception_Type__c,Severity__c,Source_Id__c
            FROM DD_Error_Log__c limit 1];

        System.assertEquals('001', log.Source_Id__c);
        System.assertEquals(String.valueOf(DD_Logger.SEVERITY_TYPES.ERROR), log.Severity__c);
        System.assertNotEquals(null, log.Exception_Type__c);
        System.assertNotEquals(null, log.Exception_Stack_Trace__c);
        System.assertNotEquals(null, log.Description__c);
        System.assertEquals('myCat', log.Category__c);
        System.assert(String.valueOf(log.Apex_Class__c).contains('DD_Logger'));
    }
}