/**
* @File Name:   DD_AbstractTriggerHandler.cls
* @Description:   
* @Author:      Fan Yang, fanxyang@deloitte.ca
* @Group:       Apex
* @Last Modified by:   Fan Yang
* @Last Modified time: 2017-05-09 17:22:29
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver       Date        Author      Modification
* 1.0       2017-05-09  Fan Yang    Created the file/class
*/
public with sharing abstract class DD_AbstractTriggerHandler implements DD_TriggerInterface{

    public virtual void beforeBulk(){}

    public virtual void afterBulk(){} 

    public virtual void beforeInsert(){}
 
    public virtual void beforeUpdate(){}

    public virtual void beforeDelete(){}

    public virtual void afterInsert(){}

    public virtual void afterUpdate(){}

    public virtual void afterDelete(){}

    public virtual void afterUndelete(){}

    public virtual void andFinally(){}
    

    /**
    * @Name          debugDatabaseResult
    * @Description
    * @Author        Fan Yang
    * @CreatedDate   2017-05-09
    * @Param         List<Database.SaveResult> results
    * @Return        void
    */
    public void debugDatabaseResult(List<Database.SaveResult> results){
        debugDatabaseResultStatic(results);
    }

    /**
    * @Name          debugDatabaseResult
    * @Description
    * @Author        Fan Yang
    * @CreatedDate   2017-05-09
    * @Param         List<Database.DeleteResult> results
    * @Return        void
    */
    public void debugDatabaseResult(List<Database.DeleteResult> results){
        debugDatabaseResultStatic(results);
    }    


    /****************************************************************************/
    /**********************************Static Methods****************************/
    /****************************************************************************/
    /**
    * @Name          debugDatabaseResultStatic
    * @Description   Output debug for database operations
    * @Author        Fan Yang
    * @CreatedDate   2017-05-09
    * @Param         List<Database.SaveResult> results
    * @Return        void
    */
    public static void debugDatabaseResultStatic(List<Database.SaveResult> results){
        for (Integer i=0; i < results.size(); i++) {
            if (!results[i].isSuccess()) {
                String error = String.valueOf(i+1) + '. ';
                for(Database.Error err : results[i].getErrors()) {                             
                    error += err.getMessage() + '; ';
                }
                System.debug('Database Error: ' + error);
            }
        }
    }

    /**
    * @Name          debugDatabaseResultStatic
    * @Description   Output debug for database operations
    * @Author        Fan Yang
    * @CreatedDate   2017-05-09
    * @Param         List<Database.DeleteResult> results
    * @Return        void
    */
    public static void debugDatabaseResultStatic(List<Database.DeleteResult> results){
        for (Integer i=0; i < results.size(); i++) {
            if (!results[i].isSuccess()) {
                String error = String.valueOf(i+1) + '. ';
                for(Database.Error err : results[i].getErrors()) {                             
                    error += err.getMessage() + '; ';
                }
                System.debug('Database Error: ' + error);
            }
        }
    }
}