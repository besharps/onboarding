/**
* @File Name:   DD_TriggerErrorHandler.cls
* @Description:   
* @Author:      Fan Yang, fanxyang@deloitte.ca
* @Group:       Apex
* @Last Modified by:   Fan Yang
* @Last Modified time: 2017-05-09 17:36:16
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver       Date        Author      Modification
* 1.0       2017-05-09  Fan Yang    Created the file/class
*/
public class DD_TriggerErrorHandler{
        public String srcObjField{get;set;} //field of trigger object that related to dmlObjField
        public String dmlObjField{get;set;} //field of new insert/update object with same value of srcObjField
        public List<sObject> dmlList{get;set;} //list new insert objects
        public List<Database.SaveResult> results{get;set;}//database result
        public Boolean triggerOld{get;set;}//insert, update or delete
        public Map<Id, sObject> sourceMap{get;set;}//dmlObjId srcObj map

        public DD_TriggerErrorHandler(List<Database.SaveResult> results){
            this.results = results;
            srcObjField = 'Id';
            triggerOld = false;
        }

        public DD_TriggerErrorHandler(List<Database.SaveResult> results, List<sObject> dmlList, Map<Id, sObject> sourceMap){
            this.results = results;
            this.dmlList = dmlList;
            this.sourceMap = sourceMap;
            srcObjField = 'Id';
            triggerOld = false;
        }

        /**
        * @Name          exec
        * @Description   Handle trigger error when dml object is not trigger object
        * @Author        Fan Yang
        * @CreatedDate   2017-05-09
        * @Return        void
        */
        public void exec(){
            if(this.sourceMap != null){//if source map provided, look source to add error
                execWithSourceMap();
            }
            else{
                execWithoutSourceMap();
            }
        }

        /**
        * @Name          execWithSourceMap
        * @Description   Handle trigger error
        * @Author        Fan Yang
        * @CreatedDate   2017-05-09
        * @Return        void
        */
        private void execWithSourceMap(){
            for (Integer i=0;i<results.size();i++) {
                if (!results[i].isSuccess()) {
                    //append all error from each field
                    String error = String.valueOf(i+1) + '. ';
                    for(Database.Error err : results[i].getErrors()) {                             
                        error += err.getMessage() + '; ';
                    }

                    Id dmlId = (Id)dmlList[i].get('Id');
                    sObject src = sourceMap.get(dmlId);
                    if(src != null){
                        src.addError(error);
                    }else{
                        System.debug('Warning - cannot find source object of trigger, id - ' + dmlId);
                    }
                }
            }
        }


        /**
        * @Name          execWithoutSourceMap
        * @Description   Handle trigger error when dml object is not trigger object
        * @Author        Fan Yang
        * @CreatedDate   2017-05-09
        * @Return        void
        */
        private void execWithoutSourceMap(){
            Map<String, String> idError = new Map<String, String>();//triggerObjId(dmlObjField) error map
            for (Integer i=0;i<results.size();i++) {
                if (!results[i].isSuccess()) {
                    //append all error from each field
                    String error = String.valueOf(i+1) + '. ';
                    for(Database.Error err : results[i].getErrors()) {                             
                        error += err.getMessage() + '; ';
                    }
                    //append all child error for the parent 
                    if (idError.containsKey(String.valueOf(dmlList[i].get(dmlObjField)))){
                        idError.put(String.valueOf(dmlList[i].get(dmlObjField)), idError.get(String.valueOf(dmlList[i].get(dmlObjField))) + '|' + error);
                    }
                    else {
                        idError.put(String.valueOf(dmlList[i].get(dmlObjField)), error);
                    }
                }
            }

            if(Trigger.new != null){
                if(!triggerOld){
                    for(SObject obj : Trigger.new) { 
                        if (idError.containsKey(String.valueOf(obj.get(srcObjField)))){
                            obj.addError(idError.get(String.valueOf(obj.get(srcObjField))));
                        }
                    }
                }else{
                    for(SObject obj : Trigger.old) { 
                        if (idError.containsKey(String.valueOf(obj.get(srcObjField)))){
                            obj.addError(idError.get(String.valueOf(obj.get(srcObjField))));
                        }
                    }
                }
            }
            
        }        
    }