/**
* @File Name:   DD_AbstractTriggerHandler_Test.cls
* @Description:   
* @Author:      Fan Yang, fanxyang@deloitte.ca
* @Group:       Apex
* @Last Modified by:   Fan Yang
* @Last Modified time: 2017-05-09 17:34:20
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver       Date        Author      Modification
* 1.0       2017-05-09  Fan Yang    Created the file/class
*/

@isTest
private class DD_AbstractTriggerHandler_Test
{   

    @isTest
    static void testDebugDML()
    {
        Test.startTest();
        DD_ConcreteTriggerHandler handler = new DD_ConcreteTriggerHandler();
        Account acc = new Account(Name='xyz');
        Database.SaveResult saveResult = Database.insert(acc, false);
        System.assertNotEquals(null, saveResult);
        handler.debugDatabaseResult(new List<Database.SaveResult>{saveResult});

        //insert twice, won't succeed
        saveResult = Database.insert(acc, false);
        System.assertNotEquals(null, saveResult);
        handler.debugDatabaseResult(new List<Database.SaveResult>{saveResult});


        Database.DeleteResult delResult = Database.delete(acc, false);
        System.assertNotEquals(null, delResult);
        handler.debugDatabaseResult(new List<Database.DeleteResult>{delResult});

        //delete twice, won't succeed
        delResult = Database.delete(acc, false);
        System.assertNotEquals(null, delResult);
        handler.debugDatabaseResult(new List<Database.DeleteResult>{delResult});

        handler.beforeBulk();
        handler.afterBulk();
        handler.beforeInsert();
        handler.beforeUpdate();
        handler.beforeDelete();
        handler.afterInsert();
        handler.afterUpdate();
        handler.afterDelete();
        handler.afterUndelete();
        handler.andFinally();

        Test.stopTest();
    }

    @isTest static void testTriggerErrorHandler(){
        Test.startTest();
        DD_ConcreteTriggerHandler handler = new DD_ConcreteTriggerHandler();
        Account acc = new Account(Name='xyz');
        List<Database.SaveResult> saveResult = new List<Database.SaveResult>{Database.insert(acc, false)};
        System.assertNotEquals(null, saveResult);
        new DD_TriggerErrorHandler(saveResult).exec();
        new DD_TriggerErrorHandler(saveResult, new List<sObject>(), new Map<Id, sObject>()).exec();
    }

    private class DD_ConcreteTriggerHandler extends DD_AbstractTriggerHandler{}
}