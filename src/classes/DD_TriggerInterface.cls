/**
* @File Name:   DD_TriggerInterface.cls
* @Description:   
* @Author:      Fan Yang, fanxyang@deloitte.ca
* @Group:       Apex
* @Last Modified by:   Fan Yang
* @Last Modified time: 2017-05-09 17:20:34
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver       Date        Author      Modification
* 1.0       2017-05-09  Fan Yang    Created the file/class
*/
public interface DD_TriggerInterface
{
    void beforeBulk();

    void afterBulk(); 

    void beforeInsert();
 
    void beforeUpdate();

    void beforeDelete();

    void afterInsert();

    void afterUpdate();

    void afterDelete();

    void afterUndelete();

    void andFinally();

}