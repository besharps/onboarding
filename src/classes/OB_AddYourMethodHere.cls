/**
* @File Name:   OB_AddYourMethodHere
* @Description: OB DevOps trianing class
* @Date Created :   2018-03-10
* @Author:      yiming yan
* @Group:       cltr
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver       Date        Author          Modification
* 1.0       2018-03-06  yiming yan      Created the class  (US-001)
*/
public with sharing class OB_AddYourMethodHere {
	public OB_AddYourMethodHere() {}

  /**
  * @Name          yimingYan
  * @Description   shell
  * @Author        yiming yan
  * @CreatedDate   2018-03-10
  * @Parameter     str a JSON string
  * @Return        void
    */
  public void yimingYan(String str){
        //hi
    }
    
  /**
  * @Name          behtaostowan
  * @Description   shell
  * @Author         behta ostowan
  * @CreatedDate   2018-03-10
  * @Parameter     str a JSON string
  * @Return        void
    */
  public void behtaOstowan(String str){
        //hi
    }

  /**
  * @Name          kevinTsai
  * @Description   shell
  * @Author        Kevin Tsai
  * @CreatedDate   2018-03-12
  * @Parameter     str a JSON string
  * @Return        void
    */
  public void kevinTsai(String str){
        //hi
    }

}