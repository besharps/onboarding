/**
* @File Name:   DD_AbstractTestBuilder.cls
* @Description:   
* @Author:      William Lopez, wilopez@deloitte.ca
* @Group:       Apex
* @Last Modified by:   Kevin Tsai
* @Last Modified time: 2017-02-19 11:21:27
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver       Date        Author      Modification
* 1.0       2018-02-19  Kevin Tsai    Created the file/class
*/

public virtual with sharing class DD_AbstractTestBuilder{

    public DD_AbstractTestBuilder(){
        //TODO. Set any default test flag.
	}
}