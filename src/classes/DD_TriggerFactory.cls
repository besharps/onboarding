/**
* @File Name:   DD_TriggerFactory.cls
* @Description:   
* @Author:      Fan Yang, fanxyang@deloitte.ca
* @Group:       Apex
* @Last Modified by:   Fan Yang
* @Last Modified time: 2017-05-09 17:27:09
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver       Date        Author      Modification
* 1.0       2017-05-09  Fan Yang    Created the file/class
*/
public without sharing class DD_TriggerFactory {//System level, sharing rules for the current user are not enforced
    @TestVisible
    private static Map<String, boolean> TriggerSequencer = null;
    @TestVisible
    private static Map<Schema.sObjectType, String> TriggerHanlderMap = null;

    static{
        TriggerHanlderMap = new Map<Schema.sObjectType, String>();
        //TriggerHanlderMap.put(Contact.sObjectType, 'DD_ContactTriggerHandler');
        for(DD_TriggerConfig tc : DD_TriggerConfig.Configurations.values()){
            TriggerHanlderMap.put(tc.SObjectType, tc.Handler);
        }
    }
    
    /**
    * @Name          getHandler
    * @Description   Get trigger handler for sObject
    * @Author        Fan Yang
    * @CreatedDate   2017-05-09
    * @Param         Schema.sObjectType soType
    * @Return        WCB_TriggerInterface
    */
    private static DD_TriggerInterface getHandler(Schema.sObjectType soType)
    {
        if(TriggerHanlderMap.containsKey(soType)){
            String className = TriggerHanlderMap.get(soType);
            return (DD_TriggerInterface)Type.forName(className).newInstance();
        }

        return null;
    } 

    /**
    * @Name          isTriggerEnabled
    * @Description   Check Trigger Status
    * @Author        Fan Yang
    * @CreatedDate   2017-05-09
    * @Param         Schema.sObjectType soType
    * @Return        Boolean
    */
    private static Boolean isTriggerEnabled(Schema.sObjectType soType){
        DD_TriggerConfig cfg = DD_TriggerConfig.getBySObjectType(soType);
        if(cfg == null) 
            return true; //default trigger is enabled
        
        return cfg.IsActive;
    }
    
    /**
    * @Name          CreateHandlerAndExecute
    * @Description   Create Trigger handler and execute
    * @Author        Fan Yang
    * @CreatedDate   2017-05-09
    * @Param         Schema.sObjectType soType
    * @Return        void
    */
    public static void CreateHandlerAndExecute(Schema.sObjectType soType)
    {
        DD_TriggerInterface handler = getHandler(soType);
        if(handler == null)
        {
            throw new TriggerException ('No trigger handler registered for Object Type: ' + soType);
        }

        if(isTriggerEnabled(soType)) //check if trigger is enabled
            execute(handler);
    }

    /**
    * @Name          execute
    * @Description   Get trigger handler for sObject
    * @Author        Fan Yang
    * @CreatedDate   2017-05-09
    * @Param         WCB_TriggerInterface handler
    * @Return        void
    */
    private static void execute(DD_TriggerInterface handler)
    {
        if(Trigger.isBefore)
        {
            handler.beforeBulk();
            
            if(Trigger.isDelete)
            {
               handler.beforeDelete();
            }
            else if(Trigger.isInsert)
            {
               handler.beforeInsert();
            }
            else if(Trigger.isUpdate)
            {
               handler.beforeUpdate();
            }
        }
        else
        {
            handler.afterBulk();
            
            if(Trigger.isDelete)
            {
               handler.afterDelete();
            }
            else if(Trigger.isInsert)
            {
               handler.afterInsert();
            }
            else if(Trigger.isUpdate)
            {
               handler.afterUpdate();
            }
            else if(Trigger.isUndelete)
            {
               handler.afterUndelete();
            }
        }

        handler.andFinally();
    }
    
    /**
    * @Name          ValidateFirstRun
    * @Description   Check if it's the first time running a trigger
    * @Author        Fan Yang
    * @CreatedDate   2017-05-09
    * @Param         Object handler
    * @Return        boolean
    */
    public static boolean ValidateFirstRun(Object handler)
    {
        boolean retVal = true;

        if (TriggerSequencer == null)
            TriggerSequencer  = new map<String, Boolean>();
        
        if (TriggerSequencer.containsKey(GetClassName(handler)))
                retVal   = false;
    
        TriggerSequencer.put(GetClassName(handler),true);
        
        return retVal;
    }

    /**
    * @Name          GetClassName
    * @Description   Get Class Name
    * @Author        Fan Yang
    * @CreatedDate   2017-05-09
    * @Param         Object handler
    * @Return        string
    */
    private static string GetClassName(Object handler)
    {
        string retVal = '';
        for (string objectName : string.valueof(handler).split(':'))
        {
            retVal = objectName;
            break;
        }
        return retVal;
    }

    /**
    * @Name          TriggerException
    * @Description   Trigger Exception Class
    * @Author        Fan Yang
    * @CreatedDate   2017-05-09
    */
    public class TriggerException extends Exception{}
}